import { CreateVoteDto } from './dto/createVote.dto';
import { Voter } from './voter.entity';
import { CreateVoterDto } from './dto/createVoter.dto';
import { VoterService } from './voter.service';
import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  Param,
  ParseIntPipe,
} from '@nestjs/common';

@Controller('voter')
export class VoterController {
  constructor(private voterService: VoterService) {}

  @Post()
  async addVoter(
    @Body(ValidationPipe) createVoterDto: CreateVoterDto,
  ): Promise<Voter[]> {
    return await this.voterService.addVoter(createVoterDto);
  }

  @Post('/:electionId')
  async vote(
    @Param('electionId', ParseIntPipe) electionId: number,
    @Body() createVoteDto: CreateVoteDto,
  ) {
    await this.voterService.vote(createVoteDto, electionId);
  }
}
