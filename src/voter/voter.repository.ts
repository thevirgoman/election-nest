import { CreateVoteDto } from './dto/createVote.dto';
import { Election } from './../election/election.entity';
import { Voter } from './voter.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Voter)
export class VoterRepository extends Repository<Voter> {
  constructor() {
    super();
  }

  async checkIfVoterElectionExists(
    addresses: string[],
    election: Election,
  ): Promise<string[]> {
    const newAddresses: string[] = [];

    addresses.forEach(async address => {
      const result = await this.find({
        relations: ['elections'],
        where: { elections: election },
      });

      if (!result) newAddresses.push(address);
    });

    return newAddresses;
  }

  async addVoter(addresses: string[], election: Election): Promise<Voter[]> {
    const voters: Voter[] = [];

    for (const address of addresses) {
      try {
        let voter = await this.findOne({ where: { address } });

        if (voter) {
          voter.elections
            ? (voter.elections = [...voter.elections, election])
            : (voter.elections = [election]);
        } else {
          voter = new Voter();
          voter.address = address;
          voter.elections = [election];
          await voter.save();
        }

        voters.push(voter);
      } catch (error) {
        throw error;
      }
    }

    return voters;
  }

  async vote(address: string, election: Election): Promise<boolean> {
    try {
      const voter: Voter[] = await this.find({ relations: ['elections'] });

      const electionExist = voter.find(el => el.elections.includes(election));

      if (!electionExist) return false;

      return true;
    } catch (error) {
      throw error;
    }
  }
}
