import { IsNumber, IsArray } from 'class-validator';

export class CreateVoterDto {
  @IsArray()
  addresses: string[];

  @IsNumber()
  electionId: number;
}
