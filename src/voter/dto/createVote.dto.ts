import { IsString, IsNumber } from 'class-validator';

export class CreateVoteDto {
  @IsString()
  address: string;

  @IsNumber()
  option: number;

  @IsNumber()
  electionId: number;
}
