import { ElectionRepository } from './../election/election.repository';
import { VoterRepository } from './voter.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { VoterController } from './voter.controller';
import { VoterService } from './voter.service';

@Module({
  imports: [TypeOrmModule.forFeature([VoterRepository, ElectionRepository])],
  controllers: [VoterController],
  providers: [VoterService],
})
export class VoterModule {}
