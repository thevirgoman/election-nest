import { CreateVoteDto } from './dto/createVote.dto';
import { Voter } from './voter.entity';
import { Election } from './../election/election.entity';
import { ElectionRepository } from './../election/election.repository';
import { CreateVoterDto } from './dto/createVoter.dto';
import { VoterRepository } from './voter.repository';
import { election as electionContract } from './../network/tron/election/index';
import * as crypto from 'crypto-js';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';

@Injectable()
export class VoterService {
  constructor(
    private voterRepository: VoterRepository,
    private electionRepository: ElectionRepository,
  ) {}

  async addVoter(createVoterDto: CreateVoterDto): Promise<Voter[]> {
    const { addresses, electionId } = createVoterDto;
    try {
      const election: Election = await this.electionRepository.findOne(
        electionId,
      );
      if (!election) throw new NotFoundException('انتخابات مورد نظر پیدا نشد.');

      //Store On the blockchain
      try {
        addresses.forEach(async address => {
          const tx = await electionContract.addVoter(
            election.hashedTitle,
            address,
          );
          console.log(tx);
        });
      } catch (error) {
        throw new BadRequestException('failed to add to contract');
      }
      try {
        const voters = await this.voterRepository.addVoter(addresses, election);

        return voters;
      } catch (error) {
        throw error;
      }
    } catch (error) {
      throw error;
    }
  }

  async vote(createVoteDto: CreateVoteDto, electionId: number) {
    const { address } = createVoteDto;

    try {
      const election: Election = await this.electionRepository.findOne(
        electionId,
      );
      if (!election) throw new NotFoundException('انتخابات مورد نظر پیدا نشد.');
      const allowed: boolean = await this.voterRepository.vote(
        address,
        election,
      );

      if (allowed) {
      } else {
        throw new BadRequestException(
          'شما مجاز به شرکت در این انتخابات نمی باشید.',
        );
      }
    } catch (error) {
      throw error;
    }
  }
}
