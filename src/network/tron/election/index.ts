import getContract from '../core/getContract';

export class election {
  static async changeAdmin(address: string): Promise<string> {
    const contract = await getContract();
    const result = await contract.changeAdmin(address).send();
    return result;
  }

  static async addElection(
    title: string,
    options: string[],
    deadLine: number,
  ): Promise<string> {
    const contract = await getContract();
    const result = await contract.addElection(title, options, deadLine).send();
    return result;
  }

  static async addVoter(
    electionTitle: string,
    address: string,
  ): Promise<string> {
    const contract = await getContract();
    const result = await contract.addVoter(electionTitle, address).send();
    return result;
  }

  static async vote(
    electionTitle: string,
    optionIndex: number,
  ): Promise<string> {
    const contract = await getContract();
    const result = await contract.vote(electionTitle, optionIndex).send();
    return result;
  }

  static async terminateContract(electionTitle: string): Promise<string> {
    const contract = await getContract();
    const result = await contract.terminateContract(electionTitle).send();
    return result;
  }

  static async getElectionDetails(electionTitle: string): Promise<string> {
    const contract = await getContract();
    const result = await contract.getElectionDetails(electionTitle).call();
    return result;
  }

  static async getOptionCount(
    electionTitle: string,
    optionIndex: number,
  ): Promise<string> {
    const contract = await getContract();
    const result = await contract
      .getOptionCount(electionTitle, optionIndex)
      .call();
    return result;
  }
}
