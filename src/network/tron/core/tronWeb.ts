import * as TronWeb from 'tronweb';
import * as config from 'config';

function tronweb() {
  const tronWeb = new TronWeb({
    fullHost: config.get('tron.fullHost'),
    privateKey: config.get('tron.privateKey'),
  });

  return tronWeb;
}

export const convertToHex = async (address: string) => {
  const tw = await tronweb();
  return tw.address.fromHex(address);
};

export default tronweb;
