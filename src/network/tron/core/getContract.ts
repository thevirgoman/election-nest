import tronWeb from './tronWeb';
import * as config from 'config';

export default async function() {
  const newTronWeb = await tronWeb();

  let address = config.get('tron.contractAddress');
  address = newTronWeb.address.fromHex(address);

  return newTronWeb.contract().at(address);
}
