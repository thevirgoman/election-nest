import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { ElectionModule } from './election/election.module';
import { VoterModule } from './voter/voter.module';
import { AdminModule } from './admin/admin.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    ElectionModule,
    VoterModule,
    AdminModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
