import { CreateAdminDto } from './dto/admin.dto';
import { Admin } from './admin.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Admin)
export class AdminRepository extends Repository<Admin> {
  constructor() {
    super();
  }

  async addAdmin(createAdminDto: CreateAdminDto): Promise<string> {
    return '';
  }
}
