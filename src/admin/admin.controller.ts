import { CreateAdminDto } from './dto/admin.dto';
import { AdminService } from './admin.service';
import { Controller, Post, Body, ValidationPipe, Get } from '@nestjs/common';

@Controller('admin')
export class AdminController {
  constructor(private adminService: AdminService) {}

  @Post()
  async changeAdmin(
    @Body(ValidationPipe) createAdminDto: CreateAdminDto,
  ): Promise<string> {
    return await this.adminService.addAdmin(createAdminDto);
  }
}
