import { CreateAdminDto } from './dto/admin.dto';
import { AdminRepository } from './admin.repository';
import { Injectable } from '@nestjs/common';
import { election as electionContract } from './../network/tron/election/index';

@Injectable()
export class AdminService {
  constructor(private adminRepository: AdminRepository) {}

  async addAdmin(createAdminDto: CreateAdminDto): Promise<string> {
    return await this.adminRepository.addAdmin(createAdminDto);
  }
}
