import { Status } from './enum/status.enum';
import { Voter } from './../voter/voter.entity';
import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  CreateDateColumn,
} from 'typeorm';

@Entity()
export class Election extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ unique: true })
  hashedTitle: string;

  @Column()
  deadLine: number;

  @Column({ default: Status.STARTED })
  status: Status;

  @Column()
  transaction: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @ManyToMany(
    type => Voter,
    voter => voter.elections,
  )
  voter: Voter[];
}
