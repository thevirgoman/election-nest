import { Status } from './enum/status.enum';
import { CreateElectionDto } from './dto/createElection.dto';
import { Election } from './election.entity';
import { EntityRepository, Repository } from 'typeorm';
import * as crypto from 'crypto-js';

@EntityRepository(Election)
export class ElectionRepository extends Repository<Election> {
  constructor() {
    super();
  }

  async addElection(
    createElectionDto: CreateElectionDto,
    transaction: string,
  ): Promise<Election> {
    const { title, deadLine } = createElectionDto;
    let titleHash = crypto.SHA256(title);
    titleHash = titleHash.toString();

    const election = new Election();
    election.title = title;
    election.hashedTitle = titleHash;
    election.deadLine = deadLine;
    election.transaction = transaction;
    try {
      await election.save();
      return election;
    } catch (error) {
      throw error;
    }
  }

  async getAll(): Promise<Election[]> {
    const query = this.createQueryBuilder('election');
    return query.getMany();
  }

  async terminateElection(election: Election): Promise<Election> {
    election.status = Status.TERMINATED;
    try {
      election.save();
      return election;
    } catch (error) {
      throw error;
    }
  }
  async getElection(id: number): Promise<Election> {
    return await this.findOne(id);
  }

  async findByHashedTitle(hashedTitle: string): Promise<Election | null> {
    return await this.findOne({ hashedTitle });
  }
}
