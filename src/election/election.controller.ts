import { CreateElectionDto } from './dto/createElection.dto';
import { ElectionService } from './election.service';
import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { Election } from './election.entity';

@Controller('election')
export class ElectionController {
  constructor(private electionService: ElectionService) {}

  @Post()
  async addElection(
    @Body() createElectionDto: CreateElectionDto,
  ): Promise<Election> {
    return await this.electionService.addElection(createElectionDto);
  }

  @Post('/:id/terminate')
  async terminateElection(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Election> {
    return await this.electionService.terminateElection(id);
  }

  @Get()
  async getAll(): Promise<Election[]> {
    return await this.electionService.getAll();
  }

  @Get('/:id')
  async getElectionById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Election> {
    return await this.electionService.getElectionById(id);
  }

  @Get('/:id/option/:option')
  async getOptionCount(
    @Param('id', ParseIntPipe) id: number,
    @Param('option', ParseIntPipe) option: number,
  ): Promise<any> {
    return await this.electionService.getOptionCount(id, option);
  }

  @Get('/:id/result')
  async getElectionResult(@Param('id', ParseIntPipe) id: number): Promise<any> {
    return await this.electionService.getElectionResult(id);
  }

  @Get('/full-info/:id')
  async getElectionDetails(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<any> {
    return await this.electionService.getElectionDetails(id);
  }
}
