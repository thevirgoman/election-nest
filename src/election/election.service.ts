import { ElectionDetails } from './../../dist/election/interface/election-details.interface.d';
import { Election } from './election.entity';
import { election as electionContract } from './../network/tron/election/index';
import { CreateElectionDto } from './dto/createElection.dto';
import { ElectionRepository } from './election.repository';
import { convertToHex } from '../network/tron/core/tronWeb';
import * as crypto from 'crypto-js';

import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';

@Injectable()
export class ElectionService {
  constructor(private electionRepository: ElectionRepository) {}

  async addElection(createElectionDto: CreateElectionDto): Promise<Election> {
    const { title, options, deadLine } = createElectionDto;
    try {
      let hashedTitle = crypto.SHA256(title);
      hashedTitle = hashedTitle.toString();

      const electionIsExist: Election | null = await this.electionRepository.findByHashedTitle(
        hashedTitle,
      );

      if (electionIsExist)
        throw new BadRequestException('عنوان انتخابات تکراری میباشد!');

      //Store election on the blockchain

      const transaction: string = await electionContract.addElection(
        hashedTitle,
        options,
        deadLine,
      );

      //Store on the central DB
      const election: Election = await this.electionRepository.addElection(
        createElectionDto,
        transaction,
      );
      return election;
    } catch (error) {
      throw new BadRequestException('انتخابات با این عنوان قبلا ثبت شده است.');
    }
  }

  async getAll(): Promise<Election[]> {
    return this.electionRepository.getAll();
  }

  async terminateElection(id: number): Promise<Election> {
    const election: Election = await this.electionRepository.getElection(id);
    if (!election) {
      throw new NotFoundException('انتخابات مورد نظر پیدا نشد.');
    }

    const terminatedElection: Election = await this.electionRepository.terminateElection(
      election,
    );

    return terminatedElection;
  }

  async getElectionById(id: number): Promise<Election> {
    const election: Election = await this.electionRepository.getElection(id);
    if (!election) {
      throw new NotFoundException('انتخابات مورد نظر پیدا نشد.');
    }

    return election;
  }

  async getOptionCount(id: number, option: number): Promise<any> {
    const election: Election = await this.electionRepository.getElection(id);
    if (!election) {
      throw new NotFoundException('انتخابات مورد نظر پیدا نشد.');
    }
  }

  async getElectionDetails(id: number): Promise<any> {
    try {
      const election: Election = await this.getElectionById(id);
      const electionFromTron = await electionContract.getElectionDetails(
        election.hashedTitle,
      );

      const result = {
        id: election.id,
        title: election.title,
        hashedTitle: election.hashedTitle,
        transaction: election.transaction,
        options: electionFromTron[1],
        status:
          parseInt(electionFromTron[2]) === 0 ? 'در حال انجام' : 'پایان یافته',
        voters: electionFromTron[3],
        totalVotes: parseInt(electionFromTron[4]),
        deadLine: parseInt(electionFromTron[5]),
        createdAt: parseInt(electionFromTron[6]),
      };
      console.log(result);

      return result;
    } catch (error) {
      throw error;
    }
  }

  async getElectionResult(id: number): Promise<any> {
    try {
      const electionDetails = await this.getElectionDetails(id);

      const result = [];

      for (const [index, option] of electionDetails.options.entries()) {
        const count = await electionContract.getOptionCount(
          electionDetails.hashedTitle,
          index,
        );
        result.push({
          option,
          count: parseInt(count),
        });
      }

      return result;
    } catch (error) {}
  }
}
