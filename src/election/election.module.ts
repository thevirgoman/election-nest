import { ElectionRepository } from './election.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { ElectionController } from './election.controller';
import { ElectionService } from './election.service';

@Module({
  imports: [TypeOrmModule.forFeature([ElectionRepository])],
  controllers: [ElectionController],
  providers: [ElectionService],
})
export class ElectionModule {}
