import { IsString, IsArray, IsNumber } from 'class-validator';

export class CreateElectionDto {
  @IsString()
  title: string;

  @IsArray()
  options: string[];

  @IsNumber()
  deadLine: number;
}
