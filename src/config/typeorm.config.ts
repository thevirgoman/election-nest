import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { get } from 'config';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: get('database.type'),
  host: get('database.host'),
  port: get('database.port'),
  database: get('database.name'),
  username: get('database.username'),
  password: get('database.password'),
  entities: [__dirname + './../**/*.entity.js'],
  synchronize: true,
  useUnifiedTopology: true,
};
